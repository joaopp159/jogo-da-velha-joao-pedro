const player1 = "X";
const player2 = "O";
var playTime = player1;
var gameOver = false;
var images = new Array();
var pewdiepie=0;
var tseries=0;

var sequence = [];
var actualRound = 0;
var seqPos = 0;
var gameOver = false;
var c = 0;
res=document.getElementById('tela');

preloadImages("imagens/pie.png","imagens/tseries.jpg");
atualizaTela();
inicializarEspacos();

function preloadImages(){
	for (i = 0; i < preloadImages.arguments.length; i++) {
					images[i] = new Image()
					images[i].src = preloadImages.arguments[i]
				}
}

function atualizaTela(){
	if (gameOver) { return;}
	if (playTime == player1){
		res.innerHTML="<span><h1>Vez do PewDiePie</h1></span>";
	} else{
		res.innerHTML="<span><h1>Vez do TSeries</h1></span>";
	}
}

function inicializarEspacos(){
	var espacos = document.getElementsByClassName("espaco");
	for (var i = 0; i < espacos.length; i++) {
		espacos[i].innerHTML = "<img id='p1' src='"+images[0].src+"' border='0'><img id='p2' src='"+images[1].src+"' border='0'>";
		espacos[i].getElementsByTagName('img')[0].style.display = "none";
		espacos[i].getElementsByTagName('img')[1].style.display = "none";
		espacos[i].addEventListener("click", function(){
			if (gameOver) {return;}
			if(this.getAttribute("jogada") == ""){
				if (playTime == player1) {
					this.getElementsByTagName('img')[0].style.display = "inline";
					this.setAttribute("jogada", player1);
					playTime = player2;

				}else{
					this.getElementsByTagName('img')[1].style.display = "inline";
					this.setAttribute("jogada", player2);
					playTime = player1;
				}
				atualizaTela();
				verificarVencedor();

			}

		});
	}
}

async function verificarVencedor(){


var a1 = document.getElementById("a1").getAttribute("jogada");
var a2 = document.getElementById("a2").getAttribute("jogada");
var a3 = document.getElementById("a3").getAttribute("jogada");

var b1 = document.getElementById("b1").getAttribute("jogada");
var b2 = document.getElementById("b2").getAttribute("jogada");
var b3 = document.getElementById("b3").getAttribute("jogada");

var c1 = document.getElementById("c1").getAttribute("jogada");
var c2 = document.getElementById("c2").getAttribute("jogada");
var c3 = document.getElementById("c3").getAttribute("jogada");


var vencedor = "";

if(((a1 == b1 && a1 == c1) || (a1 == a2 && a1 == a3 ) || (a1==b2 && a1 == c3 )) && a1 != ""  ){
	vencedor = a1;
}else if((b2 == b1 && b2 == b3 && b2 !="" ) || (b2==a2 && b2==c2 && b2 !="") || (b2==a3 && b2==c1 && b2!=""))
{
	vencedor = b2;

}else if(((c3==c2 && c3==c1)||(c3==a3 && c3 == b3)) && c3 != "")
{
	vencedor = c3;
}

if (vencedor != "") {
	gameOver = true;
	if(vencedor=="X"){
		res.innerHTML="<span><h1>PewDiePie ganhou</h1></span>";
		pewdiepie+=1;

	}
	else{
		res.innerHTML="<span><h1>Tseries ganhou</h1></span>";
		tseries+=1;
	}
	document.getElementById('placar').innerHTML="<span>PewDiePie "+pewdiepie+" X "+tseries+" TSeries</span>";
}
c+=1;
if(c==9 && gameOver == false){
	gameOver = true;
	res.innerHTML="<h1>Deu velha</h1>"	
}

}
function reiniciar(){
	document.getElementById("tela").innerHTML="<div><span><h1>Vez do PewDiePie</h1></span></div>";
	var espacos = document.getElementsByClassName("espaco");
	for (var i = 0; i < espacos.length; i++) {
		espacos[i].setAttribute("jogada", "");
	}
	images = new Array();
	gameOver=false;
	
	c=0;
	sequence = [];
	actualRound = 0;
	seqPos = 0;
	vencedor = "";

	preloadImages("imagens/pie.png","imagens/tseries.jpg");		
	atualizaTela();
	inicializarEspacos();
}